﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsApp1
{
    public class Pokemon
    {
        private string type;
        private string name;
        private int vitesse;
        private int pv;
        public string Type
        {
            get { return type; }
        }
        public string Name
        {
            get { return name; }
        }
        public int Vitesse
        {
            get { return vitesse; }
        }
        public int Pv
        {
            get { return pv; }
        }

        //constructeur
        public Pokemon(string leType,string leNom , int laVitesse, int lesPV)
        {
            type = leType;
            name = leNom;
            vitesse = laVitesse;
            pv = lesPV;
        }
        //nombre de degat pour une attaque basic
        public void DegatAttakBasic(int degatBasic)
        {
            pv = pv-degatBasic;
        }
        //nombre de degat pour une ataque Special
        public void DegatAttakSpecial(int degatSpecial)
        {
            pv=pv-(2*degatSpecial);
        }
        public void Vitessedupokemon(int vitessePokemon)
        {
            vitesse = vitessePokemon;
        }

        
    }
}

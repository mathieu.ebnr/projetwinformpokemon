using System.Reflection;
using WinFormsApp1.Properties;

namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        List<string> listePokemon = new List<string>();
        public Form1()
        {
            InitializeComponent();
            InitPokemon();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        //chargement des pokemons
        void InitPokemon()
        {
            //ajoute des pokemons deans la liste pokemon
            listePokemon.Add("Arcko");
            listePokemon.Add("Bulbizar");
            listePokemon.Add("Carapuce");
            listePokemon.Add("Grenousse");
            listePokemon.Add("H�ricendre");
            listePokemon.Add("Marisson");
           //listePokemon.Add("Ouisticrame");
            listePokemon.Add("Tiplouf");
            listePokemon.Add("Salam�che");
        }
        void Search(int pokemonAleatoir,int player)
        { 
            //Parcourir les pok�mons

           /*
            * foreach (string pokemon in listePokemon)
            {
                 Montre la liste des pokemons dans des MessageBox

                 MessageBox.Show(pokemon);
            }
            */
            string nomDuPokemon = listePokemon[pokemonAleatoir];
            MessageBox.Show(nomDuPokemon);

            //string res= "Resources" + "." + nomDuPokemon;
           // Bitmap  nom = new Bitmap(res);
            //pbxJ1.Image = Image.FromFile(nomDuPokemon);
            var image = (Bitmap)Properties.Resources.ResourceManager.GetObject(nomDuPokemon);
            if(player == 1)
            {
                pbxJ1.Image = image;
            }
            else
            {
                pbxJ2.Image = image;
            }
            

        }
        private void btnGenererPokemon_Click(object sender, EventArgs e)
        {
            int pokemon_j1;
            int pokemon_j2;

            // G�n�re les nombres al�atoires

            Random random = new Random();

            int aleatoir_j1 = random.Next(0, 8);
            int aleatoir_j2 = random.Next(0, 8);
            while (aleatoir_j2 == aleatoir_j1)
            {
                aleatoir_j2 = random.Next(0, 8);
            }
            //appelle la fonction qui cherche dans la liste 
            Search(aleatoir_j1,1);
            Search(aleatoir_j2,2);
        }

        private void pbxJ1_Click(object sender, EventArgs e)
        {

        }
    }
}
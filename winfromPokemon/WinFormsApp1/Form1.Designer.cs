﻿namespace WinFormsApp1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbxJ1 = new System.Windows.Forms.PictureBox();
            this.pbxJ2 = new System.Windows.Forms.PictureBox();
            this.btnGenererPokemon = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ2)).BeginInit();
            this.SuspendLayout();
            // 
            // pbxJ1
            // 
            this.pbxJ1.Location = new System.Drawing.Point(91, 63);
            this.pbxJ1.Name = "pbxJ1";
            this.pbxJ1.Size = new System.Drawing.Size(235, 242);
            this.pbxJ1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxJ1.TabIndex = 0;
            this.pbxJ1.TabStop = false;
            this.pbxJ1.Click += new System.EventHandler(this.pbxJ1_Click);
            // 
            // pbxJ2
            // 
            this.pbxJ2.Location = new System.Drawing.Point(345, 63);
            this.pbxJ2.Name = "pbxJ2";
            this.pbxJ2.Size = new System.Drawing.Size(182, 242);
            this.pbxJ2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxJ2.TabIndex = 1;
            this.pbxJ2.TabStop = false;
            // 
            // btnGenererPokemon
            // 
            this.btnGenererPokemon.Location = new System.Drawing.Point(596, 221);
            this.btnGenererPokemon.Name = "btnGenererPokemon";
            this.btnGenererPokemon.Size = new System.Drawing.Size(75, 23);
            this.btnGenererPokemon.TabIndex = 2;
            this.btnGenererPokemon.Text = "button1";
            this.btnGenererPokemon.UseVisualStyleBackColor = true;
            this.btnGenererPokemon.Click += new System.EventHandler(this.btnGenererPokemon_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnGenererPokemon);
            this.Controls.Add(this.pbxJ2);
            this.Controls.Add(this.pbxJ1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxJ2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private PictureBox pbxJ1;
        private PictureBox pbxJ2;
        private Button btnGenererPokemon;
    }
}